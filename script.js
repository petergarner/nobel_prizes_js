var myJson = [];  // used to store full table in browser memory

/* Binds search button in html and created browser accessible json data */
$(document).ready(function() {
    $( "#vals" ).click(checkVals);
    $.getJSON( "nobel.json", function( data ) {
        for ( var i = data.prizes.length-1; i >= 0; i-- ) {
            var year = data.prizes[i].year;
            var category = data.prizes[i].category;
            var laur = data.prizes[i].laureates;
            for (var x = laur.length-1; x >=0 ; x-- ) {
                var firstn = laur[x].firstname;
                var lastn = laur[x].surname;
                var share = laur[x].share;
                var id = laur[x].id
                var tempObj = {
                    year: year,
                    category: category,
                    firstname: firstn,
                    lastname: lastn,
                    share: share,
                    id: id,
                };
                JSON.stringify(tempObj); // stringified for webserver
                myJson.push(tempObj);
                tempObj = "";
            }
        }
        checkSets();
    });
})

var yearSet = new Set(); // used in checkVals

/* Creates a 'set of available prize years; used to prevent users for searching for non-existant years */
function checkSets() {
    for (var i = 0; i < myJson.length-1; i++ ) {
        yearSet.add(myJson[i].year);
    }
}

/* Sanity checks input field entris */
function checkVals() {
    if (yearSet.has(year.value) || year.value == "") {  // checks for year in set (or empty value)
        if (!isNaN(share.value) && share.value > 0 && share.value <= 4 || share.value == "") {  // checks share value is in scope and is a number
            if (eval(/^[a-zA-Z() èöéóäÉíá\'\-\.\´]+$/.test(surname.value)) || surname.value == "") { // checks input is of characters in json surname values?
                filterVals(); // accaptable values (or none) passed to main filter function.
            } else { alert("Incorrect surname format. Use international spelling including è ö é ó ä É í á, as well as apostrophes, hyphens and periods"); }
        } else { alert("Enter integer share value between 1 and 4"); }
    } else { alert("Incorrect year format or no prize data for this year"); }
}

/* (Re)creates table html */
function resetTable() {
    $("#myTable").empty(); // drops table
    $('#dynTab').html("<table id='myTable'><tr></tr></table>"); //injects html
}

function filterVals(boolVal){
    viewJson = []; // although we can remove the table it will just get refilled from viewJson in memory unless we clear that too (redundant on first run).
    resetTable(); // still need to drop drop the present table
    var filterJson = myJson; // make copy of the json stored

    /* Checks selected year operator and compares input value to list entries */
    for (i = 0; i <= myJson.length-1; i++) {
        if (year.value) {  // if no value, need to pass all on to next check in 'else' below
            if (yearBool.value == "Less") {
                if (year.value > filterJson[i].year ) {
                    makeObj(i,filterJson);  // call to makeObj creates a new viewJson
                }
            }
            if (yearBool.value == "Greater") {
                if (year.value < filterJson[i].year ) {
                    makeObj(i,filterJson);
                }
            }
            if (yearBool.value == "Equal") {
                if (year.value == filterJson[i].year ) {
                    makeObj(i,filterJson);
                }
            }
            if (yearBool.value == "Not_equal") {
                if (year.value != filterJson[i].year ) {
                    makeObj(i,filterJson);
                }
            }
        } else { makeObj(i,filterJson); } // runs if no year provided as NEEDS to go to makeObj at least once in this function to 'prime' viewJson (can skip the rest).
    }

    /* Checks selected category and compares to list entries */
    if (category.value != "all") {
        var filterJson = viewJson;  // this time we copy the present json 'view'; this is the filtered json that's the result from makeObj.
        viewJson = [];// needs to cleard as it will be repopulated after call to makeObj below
        resetTable();
        for (i = 0; i < filterJson.length; i++ ) {
            if (filterJson[i].category == category.value) {
                makeObj(i,filterJson);
            }
        }
    }

    /* Checks selected share operator and compares input value to list entries */
    if (share.value >= 1 ) {  // just a way to check for any enetered value to enter conditional
        var filterJson = viewJson;
        viewJson = [];// same 3 stage process (incl resetTable) as above
        resetTable();
        for (i = 0; i < filterJson.length; i++) {
            if (shareBool.value == "Greater") {
                if (filterJson[i].share > share.value ) {
                    makeObj(i,filterJson);
                }
            }
            if (shareBool.value == "Less") {
                if (filterJson[i].share < share.value ) {
                    makeObj(i,filterJson);
                }
            }
            if (shareBool.value == "Equal") {
                if (filterJson[i].share == share.value ) {
                    makeObj(i,filterJson);
                }
            }
            if (shareBool.value == "Not_equal") {
                if (filterJson[i].share != share.value ) {
                    makeObj(i,filterJson);
                }
            }
        }
    }

    /* checks for existing value in input filed; uses includes() to check value against all surnames */
    if (eval(/^[a-zA-Z() èöéóäÉíá\'\-\.\´]+$/.test(surname.value))) {
        console.log("surname runs");
        var filterJson = viewJson; // copy
        viewJson = [];  // clear
        resetTable();  // reset
        for (i =0; i < filterJson.length; i++) {
            var js_sur = filterJson[i].lastname;
            var js_low = js_sur.toLowerCase();
            var sur_val = surname.value;
            var sur_low = sur_val.toLowerCase();
            if (js_low.includes(sur_low)) {
                makeObj(i,filterJson); // repopulate viewJson
            }
        }
    }
    for (x = 0; x < viewJson.length; x++) {  // viewJson in filtered state (or raw) is sent to rendering via insertData call
        insertData(viewJson[x].year,viewJson[x].category,viewJson[x].firstname,viewJson[x].lastname,viewJson[x].share);
        }
     insertData("Year","Categpry","Firstname","Lastname","Share"); // headers added
}

/* viewJson holds the working copy of our json list, filtered and rendered in loop.  Always drawn from full copy myJson view filterJson withing filterVals funciton. */
var viewJson = [];

/* makeObj is used to stringify filtered data ready for the webserver. It is called each time there is a filter operation, including if there is no filter operation in the first step of filterVals. */
function makeObj(i,jlist) {
    var tempObj = {
        year: jlist[i].year,
        category: jlist[i].category,
        firstname: jlist[i].firstname,
        lastname: jlist[i].lastname,
        share: jlist[i].share,
        id: jlist[i].id,
        };
    JSON.stringify(tempObj);  // as above stringified for webserver
    viewJson.push(tempObj);
    tempObj = "";
}

/* To pass values to the html table */
function insertData(year, category, firstname, lastname, share) {
    var table = $("#myTable")[0];
    var row = table.insertRow(0);
    var cell1 = $(row.insertCell(0));
    var cell2 = $(row.insertCell(1));
    var cell3 = $(row.insertCell(1));
    var cell4 = $(row.insertCell(3));
    var cell5 = $(row.insertCell(4));
    cell1.html(year);
    cell2.html(category);
    cell3.html(firstname);
    cell4.html(lastname);
    cell5.html(share);
}
